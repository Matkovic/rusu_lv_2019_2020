ocjena = -0.1

try:
    ocjena = float(input('Upisi broj izmedju 0 i 1: '))

except ValueError:
    print("To nije broj!")
    
if (float(ocjena) < 0.0 or float(ocjena) > 1.0):
    print("Broj nije izmedju 0 i 1") 
else: 
    if(float(ocjena)>= 0.9):
        print("A")
    elif(float(ocjena)>= 0.8):
        print("B")    
    elif(float(ocjena)>= 0.7):
        print("C")
    elif(float(ocjena)>= 0.6):
        print("D")
    else:
        print("F")
