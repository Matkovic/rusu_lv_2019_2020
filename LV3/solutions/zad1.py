import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


mtcars = pd.read_csv(r'C:\Users\student\Desktop\lv3 python\LV3\resources\mtcars.csv')
print(len(mtcars))


print(mtcars[['car','mpg']].sort_values('mpg').tail(5))
print("--------------------------------------")
print(mtcars[['car','cyl','mpg']].query('cyl == 8').sort_values('mpg').head(3)) 
print("--------------------------------------")
print(mtcars.mpg[mtcars.cyl == 6].mean())
print("--------------------------------------")
print(mtcars.mpg[(mtcars.cyl == 4)&(mtcars.wt > 2)&(mtcars.wt < 2.2)].mean())
print("--------------------------------------")
print(mtcars.am[mtcars.am == 0].count())
print(mtcars.am[mtcars.am == 1].count())
print("--------------------------------------")
print(mtcars.am[(mtcars.am == 1)&(mtcars.hp > 100)].count())
print("--------------------------------------")
#short ton
print(mtcars[['wt']]*907.1874)

