import numpy as np
import matplotlib.pyplot as plt
import sklearn.neural_network as nn
from sklearn.preprocessing import StandardScaler
import sklearn.metrics as met

def generate_data(n):
    # prva klasa
    n1 = int(n / 2)
    x1_1 = np.random.normal(0.0, 2, (n1, 1));

    # x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1, 2) + np.random.standard_normal((n1, 1));
    y_1 = np.zeros([n1, 1])
    temp1 = np.concatenate((x1_1, x2_1, y_1), axis=1)

    # druga klasa
    n2 = int(n - n / 2)
    x_2 = np.random.multivariate_normal((0, 10), [[0.8, 0], [0, 1.2]], n2);
    y_2 = np.ones([n2, 1])
    temp2 = np.concatenate((x_2, y_2), axis=1)
    data = np.concatenate((temp1, temp2), axis=0)

    # permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices, :]

    return data
def plot_confusion_matrix(c_matrix):
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j) / float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(x, y),
                        horizontalalignment='center',
                        verticalalignment='center', color='green', size=20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])

    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    

np.random.seed(242)
train_data = generate_data(1500)

x1train = train_data[:, 0]
x2train = train_data[:, 1]
ytrain = train_data[:, 2]
xtrain = train_data[:, 0:2]


np.random.seed(12)
learn_data = generate_data(750)
x1learn = learn_data[:, 0]
x2learn = learn_data[:, 1]
ylearn = learn_data[:, 2]
xlearn = learn_data[:, 0:2]

scaler = StandardScaler()
scaler.fit(xtrain)
scaler.transform(xtrain)

ann = nn.MLPClassifier(hidden_layer_sizes=(20, 20, 10),
                       activation="logistic",
                       alpha=0.001,
                       batch_size=8,
                       max_iter=500)

ann.fit(xtrain, ytrain)
ypred = ann.predict(xlearn)

conf_mat = met.confusion_matrix(ylearn, ypred)
plot_confusion_matrix(conf_mat)

accuracy = met.accuracy_score(ylearn, ypred)

print(conf_mat)

tp = conf_mat[0][0]
fp = conf_mat[0][1]
fn = conf_mat[1][0]
tn = conf_mat[1][1]

misclass_rate = 1 - accuracy
precision = tp/(tp+fp)
recall = tp/(tp+fn)
specificity = tn / (tn+fp)

f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(learn_data[:,0])-0.5:max(learn_data[:,0])+0.5:.05,
                          min(learn_data[:,1])-0.5:max(learn_data[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = ann.predict_proba(grid)[:, 1].reshape(x_grid.shape)

cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)

ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz')
ax.scatter(x1learn, x2learn, c=ylearn)

plt.show()





