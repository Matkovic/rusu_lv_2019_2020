from sklearn import cluster,datasets
import numpy as np
import matplotlib.pyplot as plt

def generate_data(n_samples, flagc):  
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,centers = 4)
        cluster_std=[1.0, 2.5, 0.5, 3.0],
        random_state=random_state
    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    else:
        X = []
    return X

s = generate_data(500,5)

#plt.figure(1)
#x,y = zip(*s)
#plt.scatter(x, y)

k_means = cluster.KMeans(n_clusters=3, n_init=1)
k_means.fit(s)
boje = k_means.predict(s)
plt.figure(2)


for i in range(500):
    if boje[i] == 0:
        plt.scatter(s[i][0],s[i][1],c = 'b')
    if boje[i] == 1:
        plt.scatter(s[i][0],s[i][1],c = 'g')
    if boje[i] == 2:
        plt.scatter(s[i][0],s[i][1],c = 'y')

plt.scatter(k_means.cluster_centers_[:, 0], k_means.cluster_centers_[:, 1], c='r')
plt.show()
