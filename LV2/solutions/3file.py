import numpy as np
import matplotlib.pyplot as plt

np.random.seed(56) 
rNumbers = np.random.randint(2, size=100)

mu = 187
sigma = 2
v = np.random.normal(mu,sigma,10000)

visine_m = []

visine_z = []
for i in rNumbers:
    if i == 0:
        visine_z.append(np.random.normal(167, 7))
    else:
        visine_m.append(np.random.normal(180, 7))

plt.hist([visine_m, visine_z], color = ['yellow', 'red'])


avgm = np.average(visine_m)
avgz = np.average(visine_z)

plt.axvline(avgm, color='purple')
plt.axvline(avgz, color='green')
plt.legend(["m", "z", "avgm", "avgz"])
