import numpy as np
import matplotlib.pyplot as plt

np.random.seed(0) 
SixSidedDie = np.random.randint(low=1, high=7, size=100)
print(SixSidedDie)
plt.hist(SixSidedDie, bins=20)
